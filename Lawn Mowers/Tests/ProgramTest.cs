﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace LawnMowers.Tests
{

    // todo: use SpecFlow to write readable test

    [TestFixture]
    public class ProgramTest
    {

        [Test]
        public void Run_should_ReturnTheExpectedOutput()
        {
            string input =
@"5 5
1 2 N
LMLMLMLMM
3 3 E
MMRMMRMRRM";

            string expectedOutput =
@"1 3 N
5 1 E
";

            ICommandsInterpreter interpreter = new CommandsInterpreter(' ');
            IFleetController controller = new FleetController();

            string output = Program.Run(input, interpreter, controller);

            Assert.AreEqual(expectedOutput, output);
        }

    }
}
