﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Should;

namespace LawnMowers.Tests
{
    [TestFixture]
    public class MowerTest
    {
        private ICommandsInterpreter interpreter;

       
        [SetUp]
        public void SetUp()
        {
            char valuesSeparator = ' ';
            interpreter = new CommandsInterpreter(valuesSeparator);
        }


        [Test]
        public void ExecuteMovement_when_IsLeftOrRight_should_NotChangeThePositionXY()
        {
            // Arrange
            Position positionA = new Position(0, 0, Direction.N);
            Mower mower = new Mower(positionA);

            // Act
            mower.ExecuteMovement(Movement.Left);

            // Assert
            Position positionB = mower.Position;
            positionB.X.ShouldEqual(positionA.X);
            positionB.Y.ShouldEqual(positionA.Y);
        }

        private static readonly object[] MowerMovements =
        {
            // basic movement
            new object[] { new Position(0,0,Direction.N), Movement.Move, new Position(0,1,Direction.N) },
            new object[] { new Position(5,1,Direction.N), Movement.Right, new Position(5,1,Direction.E) },
            new object[] { new Position(3,7,Direction.N), Movement.Left, new Position(3,7,Direction.O) },
            // Move, all directions
            new object[] { new Position(10,10,Direction.N), Movement.Move, new Position(10,11,Direction.N) },
            new object[] { new Position(10,10,Direction.E), Movement.Move, new Position(11,10,Direction.E) },
            new object[] { new Position(10,10,Direction.S), Movement.Move, new Position(10,09,Direction.S) },
            new object[] { new Position(10,10,Direction.O), Movement.Move, new Position(09,10,Direction.O) },
            // Right 
            new object[] { new Position(0,0,Direction.N), Movement.Right, new Position(0,0,Direction.E) },
            new object[] { new Position(0,0,Direction.E), Movement.Right, new Position(0,0,Direction.S) },
            new object[] { new Position(0,0,Direction.S), Movement.Right, new Position(0,0,Direction.O) },
            new object[] { new Position(0,0,Direction.O), Movement.Right, new Position(0,0,Direction.N) },
            // Left 
            new object[] { new Position(0,0,Direction.N), Movement.Right, new Position(0,0,Direction.E) },
            new object[] { new Position(0,0,Direction.E), Movement.Right, new Position(0,0,Direction.S) },
            new object[] { new Position(0,0,Direction.S), Movement.Right, new Position(0,0,Direction.O) },
            new object[] { new Position(0,0,Direction.O), Movement.Right, new Position(0,0,Direction.N) },
        };

        [Test]
        [TestCaseSource("MowerMovements")]
        public void ExecuteMovement_should_ChangeThePositionAsExpected(Position startPosition, Movement movement, Position endPosition)
        {
            // Arrange
            Mower mower = new Mower(startPosition);

            // Act
            mower.ExecuteMovement(movement);

            // Assert
            Position positionB = mower.Position;
        }
                

    }


}
