﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Should;

namespace LawnMowers.Tests
{
    [TestFixture]
    public class CommandsInterpreterTest
    {
        private static readonly object[] ParsePositionTestCases = {
            new object[] { ',', "0,0,N",    new Position( 0,  0, Direction.N) },
            new object[] { ',', "1,2,N",    new Position( 1,  2, Direction.N) },
            new object[] { ',', "10,20,S",  new Position(10, 20, Direction.S) },
            new object[] { '-', "5-5-N",    new Position( 5,  5, Direction.N) },
            new object[] { '-', "1-1-E",    new Position( 1,  1, Direction.E) },
            new object[] { '-', "1-1-O",    new Position( 1,  1, Direction.O) },
        };

        [Test]
        [TestCaseSource("ParsePositionTestCases")]
        public void ParsePosition_should_ReturnTheRightPosition(char valuesSeparator, string positionText, Position expectedPosition)
        {
            // Arrange
            CommandsInterpreter interpreter = new CommandsInterpreter(valuesSeparator);

            // Act
            Position position = interpreter.ParsePosition(positionText);

            // Assert            
            position.ShouldEqual(expectedPosition);
        }


        [Test]
        [TestCase(1, 3, Direction.N, ExpectedResult = "1 3 N")]
        [TestCase(5, 1, Direction.E, ExpectedResult = "5 1 E")]
        public string PositionToText_should_ReturnTheExpectedText(int x, int y, Direction direction)
        {
            Position position = new Position(x, y, direction);

            CommandsInterpreter interpreter = new CommandsInterpreter(' ');
            string positionText = interpreter.RenderPosition(position);

            return positionText;
        }


        private static readonly string[] InputTestCases = {
            // #1
@"5 5
1 2 N
LMLMLMLMM
3 3 E
MMRMMRMRRM",
            // #2
@"10 20
1 2 N
LMLMLMLMM
3 3 E
MMRMMRMRRM"
        };

        [Test]
        public void ParseInput_should_SetLawnBoundaries()
        {
            // Arrange
            char valuesSeparator = ' ';
            string input = InputTestCases[0];

            CommandsInterpreter interpreter = new CommandsInterpreter(valuesSeparator);

            // Act
            Input settings = interpreter.ParseInput(input);

            // Assert
            settings.LawnWidth.ShouldEqual(5);
            settings.LawnHeight.ShouldEqual(5);
        }

        [Test]
        public void ParseInput_should_SetMovements()
        {
            // Arrange
            char valuesSeparator = ' ';
            string input = InputTestCases[0];

            CommandsInterpreter interpreter = new CommandsInterpreter(valuesSeparator);

            // Act
            Input settings = interpreter.ParseInput(input);

            // Assert
            Movement[] movementsOne = settings.MowerMovementsList[0];
            MovementsToString(movementsOne).ShouldEqual("LMLMLMLMM");

            Movement[] movementsTwo = settings.MowerMovementsList[1];
            MovementsToString(movementsTwo).ShouldEqual("MMRMMRMRRM");
        }

        [Test]
        public void ParseInput_should_SetInitialPositions()
        {
            // Arrange
            char valuesSeparator = ' ';
            string input = InputTestCases[0];

            CommandsInterpreter interpreter = new CommandsInterpreter(valuesSeparator);

            // Act
            Input settings = interpreter.ParseInput(input);

            // Assert
            Position positionOne = settings.MowerInitialPositions[0];
            positionOne.ShouldEqual(new Position(1, 2, Direction.N));

            Position positionTwo = settings.MowerInitialPositions[1];
            positionTwo.ShouldEqual(new Position(3, 3, Direction.E));
        }


        #region utils

        private static bool SettingsAreEqual(Input settings, Input expectedSettings)
        {
            return settings.LawnWidth == expectedSettings.LawnWidth
                && settings.LawnHeight == expectedSettings.LawnHeight;
                //&& settings.
        }
        
        private string MovementsToString(Movement[] movements)
        {
            StringBuilder output = new StringBuilder();
            foreach (var mov in movements)
            {
                output.Append(
                          mov == Movement.Move  ? 'M'
                        : mov == Movement.Left  ? 'L'
                        : mov == Movement.Right ? 'R'
                        : '?'
                    );
            }
            return output.ToString();
        }

        #endregion

    }
}
