﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LawnMowers
{
    public enum Movement
    {
        Left,
        Right,
        Move
    }

    public enum Direction
    {
        N,
        E,
        S,
        O
    }
}
