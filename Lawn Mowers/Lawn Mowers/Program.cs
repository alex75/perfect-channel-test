﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LawnMowers
{
    public class Program
    {
        static void Main(string[] args)
        {
            string input = "5 5\n"
                         + "1 2 N\n"
                         + "LMLMLMLMM\n"
                         + "3 3 E\n"
                         + "MMRMMRMRRM";

            ICommandsInterpreter interpreter = new CommandsInterpreter(' ');
            IFleetController controller = new FleetController();

            string output = Run(input, interpreter, controller);

            Console.WriteLine(output);

            Console.Write("\n\nPress ANY key to close...");
            Console.Read();
        }

        public static string Run(string inputText, ICommandsInterpreter interpreter, IFleetController controller)
        {
            Input input = interpreter.ParseInput(inputText);
            Output output = controller.Run(input);
            string outputText = interpreter.RenderOutput(output);
            return outputText;
        }
    }
}
