﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LawnMowers
{
    public class FleetController : IFleetController
    {        
        public Mower[] Mowers { get; private set; }

        public Output Run(Input settings)
        {       
            Mowers = new Mower[settings.MowerMovementsList.Length];

            for(int i = 0; i < Mowers.Length; i++)            
            {
                Position position = settings.MowerInitialPositions[i];
                Movement[] movements = settings.MowerMovementsList[i];
                Mower mower = new Mower(position);
                Mowers[i] = mower;                
                foreach (var movement in movements)
                {
                    mower.ExecuteMovement(movement);
                }
            }

            Output output = CreateOutput(); ;
            return output;
        }

        private Output CreateOutput()
        {
            var positions = Mowers.Select(m => m.Position).ToArray();
            Output output = new Output(positions);
            return output;
        }
    }
}
