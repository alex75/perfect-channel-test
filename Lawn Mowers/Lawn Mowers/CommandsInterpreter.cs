﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LawnMowers
{
    public class CommandsInterpreter : ICommandsInterpreter
    {
        private readonly char valuesSeparator;

        private readonly static Dictionary<char, Movement> movementsDictionary = new Dictionary<char, Movement>() {
            {'M', Movement.Move},
            {'R', Movement.Right},
            {'L', Movement.Left}
        };

        public CommandsInterpreter(char valuesSeparator)
        {
            this.valuesSeparator = valuesSeparator;
        }

        public Input ParseInput(string input)
        {
            // sanitize
            input = input.Replace("\r", "");

            int lawnWidth;
            int lawnHeight;
            Position[] startPositions = null;
            Movement[][] movementsList = null;

            List<Position> positions = new List<Position>(10);
            List<Movement[]> movements = new List<Movement[]>(10);

            try
            {                
                string[] rows = input.Split('\n');

                // boundaries
                string[] values = rows[0].Split(valuesSeparator);
                lawnWidth = int.Parse(values[0]);
                lawnHeight = int.Parse(values[1]);

                bool isPositionRow = true;
                foreach (string row in rows.Skip(1))
                {
                    values = row.Split(valuesSeparator);
                    if (isPositionRow)
                    {
                        positions.Add(ParsePosition(row));
                    }
                    else
                    {
                        movements.Add(ParseMovements(row));
                    }
                    isPositionRow = !isPositionRow;
                }
            }
            catch (Exception exc)
            {
                // todo: use a custom exception/do a better check of input
                throw new Exception(string.Format("Fail to parse input:\n{0}\n", input), exc);
            }

            startPositions = positions.ToArray();
            movementsList = movements.ToArray();

            Input settings = new Input(lawnWidth, lawnHeight, startPositions, movementsList);
            return settings;
        }

        public string RenderOutput(Output output)
        {
            StringBuilder outputText = new StringBuilder();
            foreach (var position in output.Positions)
            {
                string positionText = RenderPosition(position);
                outputText.AppendLine(positionText);
            }
            return outputText.ToString();
        }

        public Position ParsePosition(string input)
        {
            try
            {
                string[] values = input.Split(valuesSeparator);
                int x = int.Parse(values[0]);
                int y = int.Parse(values[1]);
                Direction direction = (Direction)Enum.Parse(typeof(Direction), values[2]);
                return new Position(x, y, direction);
            }
            catch (Exception exc)
            {
                // todo: use a custom exception/do a better check of input
                throw new Exception(string.Format("Fail to parse input for position '{0}'.", input), exc);
            }
        }

        private Movement[] ParseMovements(string input)
        {
            try
            {
                return input.ToCharArray().Select(c => movementsDictionary[c]).ToArray();
            }
            catch (KeyNotFoundException)
            {
                throw new Exception(string.Format("Check input values in \"{0}\", they must exists in ({1}).",
                    input, 
                    string.Join(", ", movementsDictionary.Keys.ToArray()))
                );
            }
        }

        public string RenderPosition(Position position)
        {
            return string.Format("{1}{0}{2}{0}{3}",
                valuesSeparator,
                position.X,
                position.Y, 
                position.Direction
                );
        }

    }
}
