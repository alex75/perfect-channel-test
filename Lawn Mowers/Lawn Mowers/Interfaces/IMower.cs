﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LawnMowers
{
    public interface IMower
    {
        Position Position { get;}
        void ExecuteMovement(Movement movement);        
    }
}
