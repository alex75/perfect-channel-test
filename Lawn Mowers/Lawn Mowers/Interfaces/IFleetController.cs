﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LawnMowers
{
    public interface IFleetController
    {
        Output Run(Input input);
    }
}
