﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LawnMowers
{
    public interface ICommandsInterpreter
    {
        Input ParseInput(string input);
        string RenderOutput(Output output);
        Position ParsePosition(string input);
        string RenderPosition(Position position);
    }
}
