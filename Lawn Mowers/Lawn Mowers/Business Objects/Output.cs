﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LawnMowers
{
    public class Output
    {
        public Position[] Positions { get; private set; }

        public Output(Position[] positions)
        {
            Positions = positions;
        }
        
    }
}
