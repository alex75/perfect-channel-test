﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LawnMowers
{
    public class Input
    {
        public int LawnWidth { get; private set; }
        public int LawnHeight { get; private set; }
        /// <summary>
        /// Each row of the list is a sequence of movements of one mower.
        /// Foe example list[0] contains the movements of the first mower.
        /// </summary>
        public Movement[][] MowerMovementsList { get; private set; }

        public Position[] MowerInitialPositions { get; private set; }

        //
        // todo: Mower initial position + movements = one object
        //

        public Input(int lawnWidth, int lawnHeight, Position[] startPositions, Movement[][] movementsList) {
            LawnWidth = lawnWidth;
            LawnHeight = lawnHeight;
            MowerInitialPositions = startPositions;
            MowerMovementsList = movementsList;
        }
    }
}
