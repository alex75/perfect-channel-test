﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LawnMowers
{
    public class Position
    {
        public Position(int x, int y, Direction direction)
        {
            X = x;
            Y = y;
            Direction = direction;
        }

        public int X { get; private set; }
        public int Y { get; private set; }
        public Direction Direction { get; private set; }


        public override bool Equals(object obj)
        {
            Position other = obj as Position;
            return 
                other != null            
                && (other.X == X && other.Y == Y && other.Direction == Direction);
        }

        public override string ToString()
        {
            return string.Format("X:{0}, Y:{1}, Dir:{2}", X, Y, Direction);
        }
    }
}
