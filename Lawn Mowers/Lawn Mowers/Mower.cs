﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LawnMowers
{
    public class Mower : IMower
    {

        private static readonly Dictionary<Direction, Direction> turnRight = new Dictionary<Direction, Direction>()
        {
            {Direction.N, Direction.E},
            {Direction.E, Direction.S},
            {Direction.S, Direction.O},
            {Direction.O, Direction.N }
        };

        private static readonly Dictionary<Direction, Direction> turnLeft = new Dictionary<Direction, Direction>()
        {
            {Direction.N, Direction.O},
            {Direction.O, Direction.S},
            {Direction.S, Direction.E},
            {Direction.E, Direction.N }
        };

        public Position Position { get; private set; }

        public Mower(Position position)
        {
            Position = position;
        }

        public void ExecuteMovement(Movement movement)
        {
            switch (movement)
            {
                case Movement.Move:
                    Move();
                    break;
                case Movement.Left:
                    TurnLeft();
                    break;
                case Movement.Right:
                    TurnRight();
                    break;
                default:
                    throw new Exception("Movement not implemented: '" + movement + "'.");
            }
        }

        #region movement methods

        private void TurnRight()
        {
            Direction newDirection = turnRight[Position.Direction];
            Position = new Position(Position.X, Position.Y, newDirection);
        }

        private void TurnLeft()
        {
            Direction newDirection = turnLeft[Position.Direction];
            Position = new Position(Position.X, Position.Y, newDirection);            
        }

        private void Move()
        {            
            int x = Position.X;
            int y = Position.Y;

            switch (Position.Direction)
            {
                case Direction.N:
                    y = Position.Y + 1;
                    break;
                case Direction.S:
                    y = Position.Y -1;
                    break;
                case Direction.E:
                    x = Position.X + 1;
                    break;
                case Direction.O:
                    x = Position.X - 1;
                    break;
            }

            // todo: check for lawn boundaries

            Position = new Position(x, y, Position.Direction);
        }

        #endregion
    }
}
